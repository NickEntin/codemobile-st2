import sublime, sublime_plugin
import dropbox_handler
import sys, os, uuid, webbrowser, datetime

class CodeMobileSetupCommand(sublime_plugin.WindowCommand):
	"""Sets up login such as dropbox authentication and uuid generation."""
	def run(self):
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		local_settings.set("LOCAL_UUID", str(uuid.uuid4()))
		sublime.status_message("CodeMobile: UUID set to %s." % local_settings.get("LOCAL_UUID"))
		sublime.save_settings("Code-ST2.sublime-settings")

		sublime.run_command("dropbox_authenticate")

		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		db_handler = dropbox_handler.DropboxHandler(local_settings)

class DropboxAuthenticateCommand(sublime_plugin.ApplicationCommand):
	def run(self):
		self.access_token = ""
		self.user_id = ""

		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		self.db_authenticator = dropbox_handler.DropboxAuthenticator(local_settings.get("APP_KEY"), local_settings.get("APP_SECRET"))

		# send the user to dropbox's web authentication
		token_url = self.db_authenticator.get_auth_url()
		webbrowser.open_new(token_url)
		sublime.active_window().show_input_panel("Please enter your authentication code from dropbox.", "", self.process_auth_code, None, self.auth_cancelled)

	def process_auth_code(self, auth_code):
		self.access_token = ""
		self.user_id = ""
		self.access_token, self.user_id = self.db_authenticator.get_auth_token_and_uid(auth_code.strip())

		global local_settings
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		local_settings.set("DB_ACCESS_TOKEN", self.access_token)
		local_settings.set("DB_USER_ID", self.user_id)

		sublime.save_settings('Code-ST2.sublime-settings')
		sublime.status_message("Dropbox logged in successfully. Access token: %s" % self.access_token)

	def auth_cancelled(self):
		sublime.message_dialog("Could not login to dropbox, please relaunch the plugin.")
		self.access_token = ""
		self.user_id = ""

class DisplayCurrentSettingsCommand(sublime_plugin.WindowCommand):
	"""Give a message_dialog with the current settings in it."""
	def run(self):
		view = self.window.active_view()
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		msg = ""
		msg += "APP_KEY: %s\n" % local_settings.get("APP_KEY")
		msg += "APP_SECRET: %s\n" % local_settings.get("APP_SECRET")
		msg += "LOCAL_UUID: %s\n" % local_settings.get("LOCAL_UUID")
		msg += "DB_ACCESS_TOKEN: %s\n" % local_settings.get("DB_ACCESS_TOKEN")
		msg += "DB_USER_ID: %s\n" % local_settings.get("DB_USER_ID")

		sublime.message_dialog(msg) 

class StageAllOpenFilesCommand(sublime_plugin.WindowCommand):
	"""Stages all open files in the current window."""
	def run(self):
		views = self.window.views()
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		staged_files = local_settings.get("staged_files")

		staging_count = 0
		for view in views:
			if view.file_name() not in staged_files:
				staged_files.append(view.file_name())
				staging_count += 1

		local_settings.set("staged_files", staged_files)
		sublime.save_settings("Code-ST2.sublime-settings")

		sublime.status_message("CodeMobile: %s new file(s) staged for upload. %s total file(s) are staged." % (staging_count, len(staged_files)))

class StageCurrentFileCommand(sublime_plugin.WindowCommand):
	"""Add the current file to the list of files that will be uploaded with the next push."""
	def run(self):
		view = self.window.active_view()
		current_fid = view.file_name()
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")

		staged_files = local_settings.get("staged_files")
		if current_fid not in staged_files:
			staged_files.append(current_fid)
			local_settings.set("staged_files", staged_files)

			sublime.save_settings("Code-ST2.sublime-settings")
			sublime.status_message("CodeMobile: 1 file staged for upload. %s total file(s) are staged." % len(staged_files))
		else:
			sublime.status_message("CodeMobile: File already staged. %s total file(s) are staged." % len(staged_files))

class DisplayStagedFilesCommand(sublime_plugin.WindowCommand):
	"""Displays a pop-up with the files that are currently staged for uploading."""
	def run(self):
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		staged_files = local_settings.get("staged_files")

		msg = "Staged Files:\n"
		for fid in staged_files:
			msg += "    " + fid + "\n"
		if len(staged_files) == 0:
			msg += "    <no files currently staged>\n"
		sublime.message_dialog(msg)

class RemoveStagedFilesCommand(sublime_plugin.WindowCommand):
	"""Remove files from the staging list."""
	def run(self):
		view = self.window.active_view()
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		self.staged_files = local_settings.get("staged_files")

		qp_list = self.staged_files[:]
		qp_list.append("Clear All Staged Files")

		self.window.show_quick_panel(qp_list, self.qp_command)

	def qp_command(self, index):
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		if index == -1:
			# user cancelled the quick panel selection
			sublime.status_message("CodeMobile: File remove cancelled.")
			return
		elif index == len(self.staged_files):
			# user selected the Clear All option
			self.staged_files = []
			sublime.status_message("CodeMobile: All files removed from staging.")
		else:
			# user selected one file to delete
			del self.staged_files[index]
			sublime.status_message("CodeMobile: 1 file removed from staging. %s file(s) total staged for upload." % len(self.staged_files))
		local_settings.set("staged_files", self.staged_files)
		sublime.save_settings("Code-ST2.sublime-settings")

class PushAllFilesCommand(sublime_plugin.WindowCommand):
	"""Push all staged files into the dropbox cloud with a destination."""
	def run(self):
		self.verify_db_setup()

		view = self.window.active_view()
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		sublime.status_message("CodeMobile: Connecting to dropbox.")
		self.db_handler = dropbox_handler.DropboxHandler(local_settings)

		self.other_devices = self.db_handler.get_other_devices()
		self.device_names = self.other_devices.keys()

		sublime.active_window().show_input_panel("What do you want to name this package?", "", self.process_package_name, None, self.abort)

	def verify_db_setup(self):
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		if local_settings.get("DB_ACCESS_TOKEN") == "":
			sublime.run_command("dropbox_authenticate")

	def process_package_name(self, package_name):
		self.package_name = package_name
		self.window.show_quick_panel(self.device_names, self.process_target_devices)

	def process_target_devices(self, sel_index):
		if sel_index == -1:
			if len(self.device_names) == 0:
				# if there were no devices found
				sublime.status_message("CodeMobile: No other devices found to push to. Operation cancelled.")
			else:
				# if the user pressed escape
				sublime.status_message("CodeMobile: File push cancelled.")
			return
		else:
			self.target_name = self.device_names[sel_index]
			self.target_ID = self.other_devices[self.target_name]
			staged_file_names = local_settings.get("staged_files")
			staged_file_objects = []
			for file_name in staged_file_names:
				fobj = open(file_name)
				staged_file_objects.append(fobj)
			upload_count = len(staged_file_names)

			self.db_handler.send_files_to(self.package_name, staged_file_objects, staged_file_names, self.target_ID)

			staged_files = []
			local_settings.set("staged_files", staged_files)
			sublime.save_settings("Code-ST2.sublime-settings")
			sublime.status_message("CodeMobile: Pushed %s file(s) to %s." % (upload_count, self.target_name))

	def abort(self):
		sublime.status_message("CodeMobile: File push cancelled.")

class PushCurrentFileCommand(sublime_plugin.WindowCommand):
	def run(self):
		self.verify_db_setup()

		self.view = self.window.active_view()
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		sublime.status_message("CodeMobile: Connecting to dropbox.")
		self.db_handler = dropbox_handler.DropboxHandler(local_settings)

		self.other_devices = self.db_handler.get_other_devices()
		self.device_names = self.other_devices.keys()
		
		# displayed to the user; they probably just want to use the current name of the file
		init_name = os.path.basename(self.view.file_name()) 

		sublime.active_window().show_input_panel("What do you want to name this package?", init_name, self.process_package_name, None, self.abort)

	def process_package_name(self, package_name):
		self.package_name = package_name
		self.window.show_quick_panel(self.device_names, self.process_target_devices)	

	def verify_db_setup(self):
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		if local_settings.get("DB_ACCESS_TOKEN") == "":
			sublime.run_command("dropbox_authenticate")

	def process_target_devices(self, sel_index):
		if sel_index == -1:
			if len(self.device_names) == 0:
				# if there were no devices found
				sublime.status_message("CodeMobile: No other devices found to push to. Operation cancelled.")
			else:
				# if the user pressed escape
				sublime.status_message("CodeMobile: File push cancelled.")
			return
		else:
			self.target_name = self.device_names[sel_index]
			self.target_ID = self.other_devices[self.target_name]

			file_name = self.view.file_name()
			file_object = open(file_name)
			self.db_handler.send_files_to(self.package_name, [file_object], [file_name], self.target_ID)
			sublime.status_message("CodeMobile: Pushed 1 file to %s." % self.target_name)

	def abort(self):
		sublime.status_message("CodeMobile: File push cancelled.")

class TestCommand(sublime_plugin.TextCommand):
	def run(self, arg):
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		print local_settings.get("LOCAL_UUID")

class LoadPackageCommand(sublime_plugin.WindowCommand):
	def run(self):
		self.verify_db_setup()

		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		self.db_handler = dropbox_handler.DropboxHandler(local_settings)

		self.addressed_packages = self.db_handler.get_addressed_packages()
		self.window.show_quick_panel(self.addressed_packages.keys(), self.process_package_selection)

	def process_package_selection(self, package_index):
		package_name = self.addressed_packages.keys()[package_index]
		package_uuid = self.addressed_packages[package_name]
		self.import_package(package_uuid)

	def import_package(self, package_uuid):
		common_prefix = self.db_handler.get_package_common_path(package_uuid)
		for view in self.window.views():
			file_path = os.path.abspath(view.file_name())
			# verify that the common prefix is the same
			if file_path[:len(common_prefix)] == common_prefix:
				# strip the common prefix from the front
				file_path = file_path[len(common_prefix):]
			else:
				continue
			if self.db_handler.file_exists(file_path):
				# get a file object for the dropbox file
				fobj = self.db_handler.get_file_download(file_path)
				new_contents = fobj.read()
				fobj.close()

				# replace the contents of the view with the new contents
				current_contents = sublime.Region(0, view.size())
				replacement_edit = self.view.begin_edit()
				view.replace(replacement_edit, current_contents, new_contents)
				view.end_edit(replacement_edit)
			else:
				continue

	def verify_db_setup(self):
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		if local_settings.get("DB_ACCESS_TOKEN") == "":
			sublime.run_command("dropbox_authenticate")

class EditDatastoreTablesCommand(sublime_plugin.WindowCommand):
	"""User flow for editing the datastore tables."""
	def run(self):
		self.verify_db_setup()
		self.local_settings = sublime.load_settings("Code-ST2.sublime-settings")

		sublime.status_message("CodeMobile: Getting datastore tables.")
		self.db_handler = dropbox_handler.DropboxHandler(self.local_settings)

		self.datastore_table_names = self.db_handler.get_datastore_tables()
		self.window.show_quick_panel(self.datastore_table_names, self.process_datastore_selection)

	def process_datastore_selection(self, index):
		if index == -1:
			# user escaped the menu
			sublime.status_message("CodeMobile: Operation aborted.")
		else:
			table_name = self.datastore_table_names[index]
			# a list of all the records in the table
			sublime.status_message("CodeMobile: Getting datastore table contents.")
			self.table_records = self.db_handler.get_table_contents(table_name)

			if table_name == "packages":
				self.packages_to_display = []
				for rec in self.table_records:
					print rec
					disp_str = ""
					try:
						record_date = rec.get("date").to_datetime_local().isoformat()
						sender_uuid = rec.get("sender")
						sender_name = self.db_handler.get_device_name(sender_uuid)
						if sender_name == self.local_settings.get("LOCAL_UUID"):
							sender_name = "<this device>"
						elif sender_name == "":
							sender_name = ""

						recipient_uuid = rec.get("recipient")
						recipient_name = self.db_handler.get_device_name(recipient_uuid)
						if recipient_name == self.local_settings.get("LOCAL_UUID"):
							recipient_name = "<this device>"
						elif recipient_name == "":
							recipient_name = ""
						disp_str = "%s: from %s to %s" % (record_date, sender_name, recipient_name)
					except:
						disp_str = "Misformatted record: %s" % str(rec)

					self.packages_to_display.append(disp_str)
				if len(self.packages_to_display) == 0:
					self.packages_to_display = ["no packages found"]
				else:
					self.packages_to_display.append("Clear all packages.")
				self.window.show_quick_panel(self.packages_to_display, self.process_package_selection)

			elif table_name == "devices":
				self.devices_to_display = []
				for rec in self.table_records:
					disp_str = ""
					try:
						device_name = rec.get("name")
						device_id = rec.get("deviceID")
						last_date = rec.get("lastActive").to_datetime_local().isoformat()

						disp_str = "%s (%s); last on %s" % (device_name, device_id, last_date)
					except:
						disp_str = "Misformatted record: %s" % str(rec)

					if device_id == self.local_settings.get("LOCAL_UUID"):
						disp_str = "<this device>"
					self.devices_to_display.append(disp_str)
				if len(self.devices_to_display) == 0:
					self.devices_to_display = ["no devices found"]
				else:
					self.devices_to_display.append("Clear all devices.")
				self.window.show_quick_panel(self.devices_to_display, self.process_device_selection)

	def process_package_selection(self, index):
		# should be called only from self.process_datastore_selection inside the if table_name==packages block
		if index == -1 or len(self.packages_to_display) == 0:
			sublime.status_message("CodeMobile: Operation aborted; no changes made.")
		elif index >= len(self.table_records):
			# if the user selected "Clear all packages."
			delete_count = 0
			for package_record in self.table_records:
				record_id = package_record.get_id()
				self.db_handler.delete_entry_from_table("packages", record_id)
				delete_count += 1
			sublime.status_message("CodeMobile: Successfully deleted %s out of %s packages." % (delete_count, len(self.table_records)))
		else:
			package_record = self.table_records[index]
			record_id = package_record.get_id()
			self.db_handler.delete_entry_from_table("packages", record_id)
			sublime.status_message("CodeMobile: Deleted 1 package.")

	def process_device_selection(self, index):
		# should be called only from self.process_datastore_selection inside the if table_name==devices block
		if index == -1 or len(self.devices_to_display) == 0:
			sublime.status_message("CodeMobile: Operation aborted; no changes made.")
		elif index >= len(self.table_records):
			# if the user selected "Clear all packages."
			delete_count = 0
			for device_record in self.table_records:
				record_id = device_record.get_id()
				self.db_handler.delete_entry_from_table("devices", record_id)
				delete_count += 1
			sublime.status_message("CodeMobile: Successfully deleted %s out of %s devices." % (delete_count, len(self.table_records)))
		else:
			device_record = self.table_records[index]
			record_id = device_record.get_id()
			self.db_handler.delete_entry_from_table("devices", record_id)
			sublime.status_message("CodeMobile: Deleted device %s" % device_record.get("name"))

	def verify_db_setup(self):
		local_settings = sublime.load_settings("Code-ST2.sublime-settings")
		if local_settings.get("DB_ACCESS_TOKEN") == "":
			sublime.run_command("dropbox_authenticate")