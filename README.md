Code-ST2
========
Sublime Text 2 plugin associated with the CodeMobile app for iOS, currently powered by dropbox.

THIS PLUGIN IS CURRENTLY UNDER DEVELOPMENT AND NOT YET SUITABLE FOR RELIABLE USAGE.
OUR APP HAS NOT YET BEEN PUBLISHED TO THE APP STORE.

Usage:
  To use the sublime plugin, when you are ready to send a file to your iPad, you first have to "stage" the file. Once you stage the file, you "push" it to the target device. See the commands section for more information.
  All commands are currently accessible through the "command-shift-P" menu and have a CodeMobile prefix. 
  Loading files from CodeMobile is currently not supported.

Commands:

Reauthenticate Dropbox-
  Logs into the web service and generates an OAuth2 access token. This access token is persitent between sessions and does not need to regenerated repeatedly. Currently sends the user to dropbox's web login in the default web browser and asks them to copy-paste their credentials.
  
Stage Current File-
  Adds the file in the active sublime view to the list of files to be pushed to the target device.

Stage All Open Files-
  Adds all files in the currently open window to the staged list.
  
Display Staged Files-
  Opens a dialog box showing the user all currently open files.
  
Remove Staged Files-
  Opens a menu allowing the user to remove files from the staging list, either one at a time or all at once.
  
Push All Staged Files-
  Opens a menu asking the user to select a device from the a list of currently active CodeMobile devices associated with this user's dropbox account. Then uploads all the files to the CodeMobile folder in the user's dropbox and tells the targeted device to download the file.
